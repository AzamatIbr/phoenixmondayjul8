package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utilities.TestBase;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class HilalUlugbek extends TestBase {

        @Test
        public void floorPlanVisibilityHilal() {
            driver.get("https://brasslanternnantucket.com/");

            //Testing "Floor Plans" section under "Rooms&Rates" tab
            Actions actions = new Actions(driver);

            //1st scenario: Floor Plan is visible under the "Rooms&Rates"

            //Open the "Rooms&Rates" drop down link  by hovering over
            WebElement roomsAndRates = driver.findElement(By.xpath("//a[@class= 'dropdown-toggle']"));
            actions.moveToElement(roomsAndRates).perform();

            //Verify if the "Floor Plans" tab is visible under the "Rooms&Rates" dropdown
            String actualText = driver.findElement(By.xpath("//*[@id=\"menu-item-2233\"]")).getText();
            String expectedText = "Floor Plans";

            Assert.assertEquals(actualText,expectedText,"Floor Plans link is not visible");

        }
        //2nd scenario: "Floor" Plan page opens when clicking

        @Test

        public void floorPlanClickableHilal() {
            driver.get("https://brasslanternnantucket.com/");

            Actions actions = new Actions(driver);
            WebElement roomsAndRates = driver.findElement(By.xpath("//a[@class= 'dropdown-toggle']"));
            actions.moveToElement(roomsAndRates).perform();
            WebElement floorPlan = driver.findElement(By.xpath("//*[@id=\"menu-item-2233\"]/a"));
            floorPlan.click();

            String floorPlansPageActualTitle = driver.findElement(By.className("page-title")).getText();
            String floorPlansPageExpectedTitle = "Floor Plans";

            Assert.assertEquals(floorPlansPageActualTitle,floorPlansPageExpectedTitle,"Floor plan page does not open");
        }
        //3rd scenario: Verifying the Floor Plan Web Page Title

        @Test
        public void pageTitleVerificationHilal () {
            driver.get("https://brasslanternnantucket.com/");

            Actions actions = new Actions(driver);
            WebElement roomsAndRates = driver.findElement(By.xpath("//a[@class= 'dropdown-toggle']"));
            actions.moveToElement(roomsAndRates).perform();
            WebElement floorPlan = driver.findElement(By.xpath("//*[@id=\"menu-item-2233\"]/a"));
            floorPlan.click();
            String expectedTitle = "Nantucket Getaways :: Top Rated B&B with Afternoon Cookies & Tea";
            String actualTitle = driver.getTitle();
            Assert.assertEquals(actualTitle, expectedTitle, "Floor Plan Web Page Title test is failed");
        }
        @Test
        public void GetToViewAllRoomsUlugbek() throws InterruptedException{
            //Thread.sleep(5000);
            driver.get("https://brasslanternnantucket.com/");

            Actions  act= new Actions(driver);

            act.moveToElement(driver.findElement(By.xpath("//*[@id=\"menu-item-2227\"]/a"))).build().perform();

            List<WebElement> listRooms=driver.findElements(By.xpath("//li[@id='menu-item-2227']/ul/li"));


            for (WebElement q: listRooms) {
                try {
                    if (q.getText().equalsIgnoreCase("View All Rooms")) {
                        q.click();
                    }
                }catch (RuntimeException r){
                    System.out.println("Exception");
                }
            }

        }

        @Test
        public void CheckingSecondLinefromTopUlugbek() {
            driver.get("https://brasslanternnantucket.com/");

            List<WebElement> listName = driver.findElements(By.xpath("//ul[@id='menu-main-menu-left']/li/a"));


            for (WebElement t : listName) {
                if (t.getText().equalsIgnoreCase("home")) {
                    System.out.println("HOME is displayed correct");
                } else if (t.getText().equalsIgnoreCase("Rooms & Rates")) {
                    System.out.println("Rooms & Rates is displayed correct");

                } else if (t.getText().equalsIgnoreCase("Specials & Packages")) {
                    System.out.println("Specials & Packages is displayed correct");
                }

            }
            System.out.println("\n" + "\n");
            List<WebElement> secondName = driver.findElements(By.xpath("//ul[@id='menu-main-menu-right']/li/a"));


            for (WebElement t : secondName) {
                if (t.getText().equalsIgnoreCase("Visit Nantucket")) {
                    System.out.println("Visit Nantucket is displayed correct");
                } else if (t.getText().equalsIgnoreCase("About")) {
                    System.out.println("About is displayed correct");

                } else if (t.getText().equalsIgnoreCase("Photo Gallery")) {
                    System.out.println("Photo Gallery is displayed correct");
                } else {
                    System.out.println("smth wrong");
                }

            }
        }
        @Test
        public void CheckingLinksandTextofFirstLineofPageUlugbek(){
            driver.get("https://brasslanternnantucket.com/");

            WebElement phone=driver.findElement(By.xpath("//a[@class='phone']"));
            System.out.println(phone.getText());
            System.out.println(phone.isDisplayed());
            if(phone.getText().equalsIgnoreCase("508.228.4064")&&phone.isDisplayed()){
                System.out.println("phone is displyed correct ");
            }else{
                System.out.println("Something wrong with phone-text");
            }
            System.out.println(phone.getAttribute("href"));
            if(phone.getAttribute("href").equalsIgnoreCase("tel:508-228-4064")&&phone.isEnabled()){
                System.out.println("phone links is Enabled");
            }else{
                System.out.println("Something wrong with Link phone");
            }

            System.out.println("\n"+"\n");

            WebElement weather = driver.findElement(By.xpath("/html/body/header/div[2]/div[1]/a[2]"));
            System.out.println(weather.getText());
            System.out.println(phone.isDisplayed());
            if(weather.getText().equalsIgnoreCase("weather")&&weather.isDisplayed()){
                System.out.println("Weather is displyed correct");
            }else{
                System.out.println("Something wrong with weather-text");
            }
            System.out.println(weather.getAttribute("href"));
            if(weather.getAttribute("href").equalsIgnoreCase("https://brasslanternnantucket.com/weather/")&&weather.isEnabled()){
                System.out.println("weather links is Enabled");
            }else{
                System.out.println("Something wrong with Link weather");
            }

            System.out.println("\n"+"\n");

            WebElement checkavailability = driver.findElement(By.xpath("/html/body/header/div[2]/div[2]/a"));
            System.out.println(checkavailability.getText());
            System.out.println(checkavailability.isDisplayed());

            if(checkavailability.getText().equalsIgnoreCase("CHECK AVAILABILITY")&&checkavailability.isDisplayed()){
                System.out.println("checkavailability is displyed correct");
            }else{
                System.out.println("Something wrong with checkavailability-text");
            }

            System.out.println(checkavailability.getAttribute("href"));
            if(checkavailability.getAttribute("href").equalsIgnoreCase("https://secure.thinkreservations.com/brasslanternnantucket/reservations")&&checkavailability.isEnabled()){
                System.out.println("weather links is Enabled");
            }else{
                System.out.println("Something wrong with Link checkavailability");
            }
        }
    }



