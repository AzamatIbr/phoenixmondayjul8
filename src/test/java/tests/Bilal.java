package tests;

        import io.github.bonigarcia.wdm.WebDriverManager;
        import org.openqa.selenium.By;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.chrome.ChromeDriver;
        import org.openqa.selenium.support.ui.Select;
        import org.testng.Assert;
        import org.testng.annotations.Test;
        import utilities.TestBase;

        import java.util.List;
        import java.util.concurrent.TimeUnit;

public class Bilal extends TestBase {

    @Test
    public void testRun1() {

        driver.get("https://brasslanternnantucket.com/");
        WebElement checkAvailabilityButton = driver.findElement(By.cssSelector("a.check-availability.tag-res-redirect"));
        checkAvailabilityButton.click();
        WebElement checkInDate = driver.findElement(By.cssSelector("input#check-in-field"));
        checkInDate.click();

        for (int i = 0; i <= 2; i++) {
            WebElement checkInDateMonthChanger = driver.findElement(By.cssSelector("a.ui-datepicker-next.ui-corner-all"));
            checkInDateMonthChanger.click();
        }

        WebElement checkInExactDay = driver.findElement(By.linkText("14"));
        checkInExactDay.click();

        WebElement checkOutDate = driver.findElement(By.cssSelector("input#check-out-field"));
        checkOutDate.click();

        WebElement checkOutExactDay = driver.findElement(By.linkText("23"));
        checkOutExactDay.click();

        WebElement childSelection = driver.findElement(By.cssSelector("select#number-of-children-field"));
        Select childDropdown = new Select(childSelection);

        String a = childDropdown.getFirstSelectedOption().getText();
        boolean check = true;
        if (!a.equals("0")) {
            check = false;
        }

        Assert.assertTrue(check, "Default number of child verification - FAILED");
    }

    @Test
    public void testRun2(){

        driver.get("https://brasslanternnantucket.com/");
        WebElement checkAvailabilityButton = driver.findElement(By.cssSelector("a.check-availability.tag-res-redirect"));
        checkAvailabilityButton.click();
        WebElement checkInDate = driver.findElement(By.cssSelector("input#check-in-field"));
        checkInDate.click();

        for (int i=0; i<=2; i++){
            WebElement checkInDateMonthChanger = driver.findElement(By.cssSelector("a.ui-datepicker-next.ui-corner-all"));
            checkInDateMonthChanger.click();
        }

        WebElement checkInExactDay = driver.findElement(By.linkText("14"));
        checkInExactDay.click();

        WebElement checkOutDate = driver.findElement(By.cssSelector("input#check-out-field"));
        checkOutDate.click();

        WebElement checkOutExactDay = driver.findElement(By.linkText("23"));
        checkOutExactDay.click();

        WebElement childSelection = driver.findElement(By.cssSelector("select#number-of-children-field"));
        Select childDropdown = new Select(childSelection);
        childDropdown.selectByValue("2");

        List<WebElement> titlesForChildrenAgesDropdowns = driver.findElements(By.xpath("//label[contains(@for, 'child-age')]"));

        int a = titlesForChildrenAgesDropdowns.size();
        boolean check = true;
        if (a!=2){
            check = false;
        }

        Assert.assertTrue(check,"Number of dropdown boxes for child age selection verification - FAILED");
    }

    @Test
    public void testRun3(){

        driver.get("https://brasslanternnantucket.com/");
        WebElement checkAvailabilityButton = driver.findElement(By.cssSelector("a.check-availability.tag-res-redirect"));
        checkAvailabilityButton.click();
        WebElement checkInDate = driver.findElement(By.cssSelector("input#check-in-field"));
        checkInDate.click();

        for (int i=0; i<=2; i++){
            WebElement checkInDateMonthChanger = driver.findElement(By.cssSelector("a.ui-datepicker-next.ui-corner-all"));
            checkInDateMonthChanger.click();
        }

        WebElement checkInExactDay = driver.findElement(By.linkText("14"));
        checkInExactDay.click();

        WebElement checkOutDate = driver.findElement(By.cssSelector("input#check-out-field"));
        checkOutDate.click();

        WebElement checkOutExactDay = driver.findElement(By.linkText("23"));
        checkOutExactDay.click();

        WebElement searchButtonForReservation = driver.findElement(By.xpath("//div[@class='search-button-wrapper']//button"));
        searchButtonForReservation.click();

        String reservation = driver.findElement(By.xpath("//div[.='Total']")).getText().trim();
        String expected = "Total";
        boolean check = true;
        if(!reservation.equals(expected)){
            check = false;
        }
        Assert.assertTrue(check, "Calculation Process for Reservation - FAILED");
    }

    @Test
    public void testRun4(){

        driver.get("https://brasslanternnantucket.com/");
        WebElement checkAvailabilityButton = driver.findElement(By.cssSelector("a.check-availability.tag-res-redirect"));
        checkAvailabilityButton.click();
        WebElement checkInDate = driver.findElement(By.cssSelector("input#check-in-field"));
        checkInDate.click();

        for (int i=0; i<=2; i++){
            WebElement checkInDateMonthChanger = driver.findElement(By.cssSelector("a.ui-datepicker-next.ui-corner-all"));
            checkInDateMonthChanger.click();
        }

        WebElement checkInExactDay = driver.findElement(By.linkText("14"));
        checkInExactDay.click();

        WebElement checkOutDate = driver.findElement(By.cssSelector("input#check-out-field"));
        checkOutDate.click();

        WebElement checkOutExactDay = driver.findElement(By.linkText("23"));
        checkOutExactDay.click();

        WebElement searchButtonForReservation = driver.findElement(By.xpath("//div[@class='search-button-wrapper']//button"));
        searchButtonForReservation.click();

        WebElement romancePackageAdditionToReservation = driver.findElement(By.xpath("(//button[.='Add to reservation'])[1]"));
        romancePackageAdditionToReservation.click();

        String packageCost = driver.findElement(By.xpath("(//div[.='$3071.38'])[1]")).getText().trim();
        String expected = "$3071.38";
        boolean check = true;
        if (!packageCost.equals(expected)){
            check = false;
        }

        Assert.assertTrue(check, "Cost Calculation for Romance Package Addition - FAILED");
    }


}
