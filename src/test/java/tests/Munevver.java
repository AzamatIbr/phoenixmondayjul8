package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.TestBase;

import java.util.List;

public class Munevver extends TestBase {

    @Test
    public void verifyingRate() throws InterruptedException {

        Actions actions = new Actions(driver);

        //1.Go to Website
        driver.get("https://brasslanternnantucket.com/");

        //2.Click on Rooms & Rates
        WebElement roomsAndRates = driver.findElement(By.xpath("//a[@class= 'dropdown-toggle']"));
        actions.moveToElement(roomsAndRates).perform();


        //3.Click on Rates& Amenities
        Thread.sleep(3000);
        WebElement ratesAndAminities = driver.findElement(By.xpath("(//body//a)[13]"));
        ratesAndAminities.click();


        //4.Verify that family suits rate is $285 - $725
        String expected = "$285 - $725";
        WebElement rate =driver.findElement(By.xpath("(//td[@class='rates-col'])[3]/span"));
        System.out.println("Family suits rate: "+ rate.getText() );
        Assert.assertTrue(rate.getText().contains(expected));
    }

    @Test
    public  void verifyingTitle() throws InterruptedException {
        Actions actions = new Actions(driver);

        //1.Go to Website
        driver.get("https://brasslanternnantucket.com/");

        WebElement roomsAndRates = driver.findElement(By.xpath("//a[@class= 'dropdown-toggle']"));
        actions.moveToElement(roomsAndRates).perform();


        //2.Click on Rates& Amenities
        Thread.sleep(3000);
        WebElement ratesAndAminities = driver.findElement(By.xpath("(//body//a)[13]"));
        ratesAndAminities.click();

        Thread.sleep(3000);

        //3.Click on family room
        WebElement familyRoom = driver.findElement(By.xpath("(//body//a)[50]"));
        familyRoom.click();

        //4.Click on Brass Lantern Inn
        WebElement brassLaternInn = driver.findElement(By.linkText("Brass Lantern Inn"));
        brassLaternInn.click();
        System.out.println("Title: "+driver.getTitle());

        Assert.assertTrue(driver.getTitle().contains("Brass Lantern Inn: Bed and Breakfast in Downtown Nantucket"));
    }

    @Test
    public void headers() throws InterruptedException {

        Actions actions = new Actions(driver);

        //1.Go to Website
        driver.get("https://brasslanternnantucket.com/");
        WebElement roomsAndRates = driver.findElement(By.xpath("//a[@class= 'dropdown-toggle']"));
        actions.moveToElement(roomsAndRates).perform();

        //2.Click on Rates& Amenities
        Thread.sleep(3000);
        WebElement ratesAndAminities = driver.findElement(By.xpath("(//body//a)[13]"));
        ratesAndAminities.click();

        //3. Print whole headers
        List<WebElement> headers = driver.findElements(By.xpath("(//table[@class='table tablesaw tablesaw-stack'])[1]/thead/tr/th"));
        for(WebElement header: headers){
            System.out.println(header.getText());
        }
    }
}
