package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.TestBase;

import java.util.List;

public class Diana extends TestBase {
    @Test
    public void petFriendlyTag(){
        driver.get("https://brasslanternnantucket.com/");
        Actions action = new Actions(driver);
        WebElement petFriendly = driver.findElement(By.xpath("//*[@id=\"menu-item-2227\"]/a"));
        action.moveToElement(petFriendly).build().perform();
        WebElement petFriendlyTag = driver.findElement(By.xpath("//*[@id=\"menu-item-2232\"]/a"));
        String actualTegText =  petFriendlyTag.getText();
        String expectedTegText = "Pet Friendly";
        petFriendlyTag.click();
        Assert.assertEquals(actualTegText, expectedTegText, "text inside of tag is not mathing");

    }


    @Test
    public void checkLinks(){
        driver.get("https://brasslanternnantucket.com/pet-friendly-nantucket-hotel.html");
        List<WebElement> links = driver.findElements(By.xpath("//body//a"));
        Assert.assertTrue(links.size() >= 70, "Print this if links don't work");

    }
    @Test
    public void pageText(){
        driver.get("https://brasslanternnantucket.com/pet-friendly-nantucket-hotel.html");
        WebElement pageTitlePage = driver.findElement(By.className("page-title"));
        Assert.assertTrue(pageTitlePage.getText().contains("Nantucket Pet Friendly Boutique Hotel"));

    }

}
