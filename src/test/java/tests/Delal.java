package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.TestBase;

public class Delal extends TestBase {

    @Test
    public void hotelReservation() {
        driver.get("https://brasslanternnantucket.com");

        WebElement checkAvailability = driver.findElement(By.xpath("/html/body/header/div[2]/div[2]"));
        checkAvailability.click();
        WebElement checkINBox = driver.findElement(By.xpath("//*[@id=\"rendered_content\"]/div/div[2]/div[2]/form/div/div[1]/div[1]/div[1]"));
        checkINBox.click();
        WebElement pickDay = driver.findElement(By.xpath("//a[.='15']"));
        pickDay.click();
        WebElement checkOutbox = driver.findElement(By.xpath("//a[.='30']"));
        checkOutbox.click();
        WebElement adualtBox = driver.findElement(By.xpath("//*[@id=\"rendered_content\"]/div/div[2]/div[2]/form/div/div[1]/div[2]/div[1]"));
        adualtBox.click();
        WebElement selectAdualt = driver.findElement(By.id("number-of-adults-field"));
        selectAdualt.sendKeys("3");

        Actions actions = new Actions(driver);
        WebElement search = driver.findElement(By.xpath("//div[@class='search-button-wrapper']/button"));
        search.click();
        String actualTitle = "//div[.='No rooms matched your search.']";
        Assert.assertTrue(actualTitle.contains("No rooms matched your search.")," Title verification failed");
    }

    @Test
    public void addToWaitingList(){
        driver.get("https://secure.thinkreservations.com/brasslanternnantucket/reservations/availability?start_date=2019-07-15&end_date=2019-07-30&number_of_adults=3&number_of_children=0&customer_group=&coupon_code=&room_id=");
        WebElement chechInFeild = driver.findElement(By.id("check-in-field"));
        chechInFeild.click();
        WebElement checkInday = driver.findElement(By.xpath("//a[.='14']"));
        checkInday.click();
        WebElement chechOutFeild = driver.findElement(By.xpath("//input[@id='check-out-field']"));
        chechOutFeild.click();
        WebElement checkOutDay = driver.findElement(By.xpath("//a[.='15']"));
        checkOutDay.click();
        WebElement adualtBox2 = driver.findElement(By.id("number-of-adults-field"));
        adualtBox2.click();
        adualtBox2.sendKeys("1");
        WebElement firstName = driver.findElement(By.xpath("//input[@id='waitlist-firstName']"));
        firstName.sendKeys("Delal");
        WebElement lastName = driver.findElement(By.xpath("//*[@id=\"waitlist-lastName\"]"));
        lastName.sendKeys("Batkin");
        WebElement phoneNumber = driver.findElement(By.id("waitlist-phone"));
        phoneNumber.sendKeys("123456789");
        WebElement emailAdders = driver.findElement(By.id("waitlist-email"));
        emailAdders.sendKeys("delal63@gmail.com");
        WebElement RoomPreference = driver.findElement(By.id("waitlist-roomPreference"));
        RoomPreference.sendKeys("Barclay");
        WebElement addToWaitList = driver.findElement(By.xpath("//*[@id=\"rendered_content\"]/div/div[1]/div[3]/div[2]/div[7]/button"));
        addToWaitList.click();
        String actualTitle2 = "//div[.='Thank you! You have been added to the wait list.']";

        Assert.assertTrue(actualTitle2.contains("Thank you! You have been added to the wait list."),"the verification failed");
    }

}
