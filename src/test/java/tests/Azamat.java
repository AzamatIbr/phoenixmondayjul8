package tests;

import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.TestBase;

public class Azamat extends TestBase {
    @Test
    public void test1AzamatCheckingDefaultValueofAdultDropDownBox(){

        driver.get("https://brasslanternnantucket.com/");

        WebElement checkAvailability = driver.findElement(By.className("hidden-xxs"));
        checkAvailability.click();

        WebElement adultsBox = driver.findElement(By.id("number-of-adults-field"));
        Select adultsDropDown = new Select(adultsBox);
        String expectedNumberOfAdultByDefault = "2";
        System.out.println("Expected number of Adults: " + expectedNumberOfAdultByDefault);
        System.out.println("Given number of Adults: " + adultsDropDown.getFirstSelectedOption().getText());
        Assert.assertTrue(adultsDropDown.getFirstSelectedOption().getText().equals(expectedNumberOfAdultByDefault),"Verification that Adults field value by default  FAILED!");




    }
    @Test
    public void test2AzamatCheckingDefaultValueofChildrenDropDownBox(){

        driver.get("https://brasslanternnantucket.com/");

        WebElement checkAvailability = driver.findElement(By.className("hidden-xxs"));
        checkAvailability.click();

        WebElement childrensBox = driver.findElement(By.id("number-of-children-field"));
        Select childrenDropDown = new Select(childrensBox);
        String expectedNumberOfChildrenByDefault = "0";
        System.out.println("");
        System.out.println("Expected number of Childrens: " + expectedNumberOfChildrenByDefault);
        System.out.println("Given number of Childrens: " + childrenDropDown.getFirstSelectedOption().getText());

        Assert.assertTrue(childrenDropDown.getFirstSelectedOption().getText().equals(expectedNumberOfChildrenByDefault),"Verification that Childrens field value by default FAILED");

    }

    @Test
    public void test3AzamatMakeSureThatOrderIsonWaitList(){

        driver.get("https://brasslanternnantucket.com/");

        WebElement checkAvailability = driver.findElement(By.className("hidden-xxs"));
        checkAvailability.click();

        WebElement checkInMonthWindow = driver.findElement(By.id("check-in-field"));
        WebElement checkout = driver.findElement(By.id("check-out-field"));
        checkInMonthWindow.click();
        WebElement july15th = driver.findElement(By.xpath("//div//tr[3]//td[2]"));
        july15th.click();
        checkout.click();
        WebElement july20th = driver.findElement(By.xpath("//div//tr[3]//td[7]"));
        july20th.click();

        WebElement adultsBox = driver.findElement(By.id("number-of-adults-field"));
        Select adultsDropDown = new Select(adultsBox);
        adultsDropDown.selectByIndex(2);
        WebElement searchButton = driver.findElement(By.xpath("//div/button[@class='search']"));
        searchButton.click();

        Faker faker = new Faker();
        WebElement firstNameWL = driver.findElement(By.id("waitlist-firstName"));
        WebElement lastNameWL = driver.findElement(By.id("waitlist-lastName"));
        WebElement phoneWL = driver.findElement(By.id("waitlist-phone"));
        firstNameWL.sendKeys(faker.name().firstName());
        lastNameWL.sendKeys(faker.name().lastName() );
        phoneWL.sendKeys(faker.phoneNumber().cellPhone());
        WebElement adToWaitListButton = driver.findElement(By.xpath("//div[@class='component add-to-wait-list']//button"));
        adToWaitListButton.click();

        WebElement approvalThatInWaitList = driver.findElement(By.xpath("//div[@class='component add-to-wait-list']/div"));
        Assert.assertTrue(approvalThatInWaitList.isDisplayed(),"Verification That User in Wait List FAILED");

    }



}
