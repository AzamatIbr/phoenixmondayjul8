package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.TestBase;

public class Tarik extends TestBase {

    @Test
    public  void checkAvailabilities() throws InterruptedException {

        driver.get("https://brasslanternnantucket.com/");

        WebElement checkAvailabilityButton = driver.findElement(By.xpath("//a[@class='check-availability tag-res-redirect']"));
        checkAvailabilityButton.click();
        WebElement checkingDteBox =driver.findElement(By.cssSelector("input#check-in-field"));
        checkingDteBox.click();
        WebElement checkingDay =driver.findElement(By.xpath("(//a[@href='#'])[21]"));
        checkingDay.click();
        WebElement checkoutBox=driver.findElement(By.id("check-out-field"));
        checkoutBox.click();
        WebElement checkoutDay=driver.findElement(By.xpath("(//a[@href='#'])[4]"));
        checkoutDay.click();
        WebElement numsOfAdult = driver.findElement(By.cssSelector("select#number-of-adults-field"));

        Select adultDropdown = new Select(numsOfAdult);

        adultDropdown.selectByValue("1");
        WebElement searchButton = driver.findElement(By.xpath("//div[@class='search-button-wrapper']//button"));
        searchButton.click();
        Thread.sleep(3000);
        String noAvailabilityMessage = driver.findElement(By.xpath("//h2[.=\"Didn't find what you were looking for?\"]")).getText().trim();
        String expected = "Didn't find what you were looking for?";

        Assert.assertEquals(noAvailabilityMessage, expected);
    }

    @Test
    public void checkAvailabilities1() throws InterruptedException {

        driver.get("https://brasslanternnantucket.com/");

        WebElement checkAvailabilityButton = driver.findElement(By.xpath("//a[@class='check-availability tag-res-redirect']"));
        checkAvailabilityButton.click();
        WebElement checkingDteBox =driver.findElement(By.cssSelector("input#check-in-field"));
        checkingDteBox.click();
        WebElement checkingDay =driver.findElement(By.xpath("(//a[@href='#'])[14]"));
        checkingDay.click();
        WebElement checkoutBox=driver.findElement(By.id("check-out-field"));
        checkoutBox.click();
        WebElement checkoutDay=driver.findElement(By.xpath("(//a[@href='#'])[4]"));
        checkoutDay.click();
        WebElement numsOfAdult = driver.findElement(By.cssSelector("select#number-of-adults-field"));

        Select adultDropdown = new Select(numsOfAdult);

        adultDropdown.selectByValue("1");
        WebElement searchButton = driver.findElement(By.xpath("//div[@class='search-button-wrapper']//button"));
        searchButton.click();
        Thread.sleep(3000);

        driver.findElement(By.xpath("//input[@id='waitlist-firstName']")).sendKeys("George Michael");
        driver.findElement(By.xpath("//input[@id='waitlist-lastName']")).sendKeys("Harlem");
        driver.findElement(By.xpath("//input[@id='waitlist-phone']")).sendKeys("1238471638");
        driver.findElement(By.xpath("//input[@id='waitlist-email']")).sendKeys("abdgh@gmail.com");
        driver.findElement(By.xpath("//input[@id='waitlist-roomPreference']")).sendKeys("12345678");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@class='actions']/button")).click();
        Thread.sleep(2000);
        WebElement result = driver.findElement(By.xpath("//div[@class='on-wait-list']"));
        String expected = "Thank you! You have been added to the wait list.";
        Assert.assertEquals(expected, result.getText());

        }
        @Test
        public void checkAvailability3() {
            driver.get("https://brasslanternnantucket.com/");
            WebElement checkAvailabilityButton = driver.findElement(By.xpath("//a[@class='check-availability tag-res-redirect']"));
            checkAvailabilityButton.click();
            Assert.assertTrue(driver.findElement(By.xpath("//input[@type='radio']")).isSelected());
        }
    }




